<?php

namespace Kavenegar\SKavenegar\Exceptions;

class HttpException extends BaseRuntimeException
{
	public function getName()
    {
        return 'HttpException';
    }
}

?>
